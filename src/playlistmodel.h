#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QAbstractListModel>

#include "mediaitem.h"
/**
  * Represents the way the data are managed in order to create a playlist.
  */
class PlaylistModel : public QAbstractListModel
{
    Q_OBJECT
public:
    PlaylistModel(QObject *parent = 0);
    ~PlaylistModel();

    //Overloaded functions
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent) const;

    /**
      * Adds a MediaItem at a certain position in the playlist.
      *
      * @param row the row where to add the MediaItem
      * @param mediaItem the MediaItem to add
      */
    void addMediaItem(int row,const MediaItem & mediaItem);
    /**
      * Adds a MediaItem at the end of the playlist.
      *
      * @param mediaItem the MediaItem to add
      */
    void enqueueMediaItem(const MediaItem & mediaItem);
    /**
      * Returns the position of the playing item in the list.
      */
    int playingItemPosition() const;
    /**
      * Sets the position of the playing item in the list.
      */
    void setPlayingItemPosition(int position);
    /**
      * Returns true if there is another item to play after the current one,
      * false otherwise.
      */
    bool hasNext() const;
    /**
      * Returns true if there is another item to play before the current one,
      * false otherwise.
      */
    bool hasPrevious() const;
    /**
      * Returns the item located after the current one  if it exists,
      * an empty one otherwise.
      */
    MediaItem next() const;
    /**
      * Returns the item located before the current one if it exists,
      * an empty one otherwise.
      */
    MediaItem previous() const;
    /**
      * Removes a MediaItem at a certain position.
      */
    void removeMediaItem(int position);
    /**
      * Retrieves a MediaItem at a certain position.
      */
    MediaItem item(int position) const;
    /**
      * Moves a MediaItem from one position to another.
      */
    void move(int origin, int destination);
    /**
      * Returns the number of items in the playlist.
      */
    int size() const;
    /**
      * Swaps two items in the playlist.
      */
    void swap(int origin, int destination);
public slots:
    /**
      * Removes all the items of the playlist.
      */
    void clear();

private:
    /**
      * Represents the list of MediaItems to play.
      */
    QList<MediaItem> m_mediaItems;
    /**
      * Represents the position of the playing MediaItem in the m_mediaItems list.
      * Its value is -1 if the playlist is empty.
      */
    int m_playingItemPosition;
};

#endif // PLAYLISTMODEL_H
