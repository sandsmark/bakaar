#include <QLabel>
#include <QApplication>
#include <QMoveEvent>
#include <QDropEvent>
#include <Phonon/AudioOutput>
#include <Phonon/BackendCapabilities>

#include <KUrl>
#include <KFileDialog>
#include <KLocale>

#include "mainwindow.h"


using namespace Phonon;

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent), m_lastMousePosition(0,0)
{
    m_mediaPlayer = new MediaPlayer(this);
    m_videoWidget = new VideoWidget(this);

    m_adjustedSize = false;
    setMouseTracking(true);//process mouseMoveEvents for OSD hiding
    setAcceptDrops(true);//allow drop event
    KUrl videoUrl;
    QList<KUrl> videoUrls;


    QStringList arguments = QApplication::arguments();
    if (arguments.length() >= 2){
        for (int i=1; i < arguments.length(); i++){
            KUrl url(arguments[i]);
            videoUrls.append(url);
        }
    } else {
        videoUrls = KFileDialog::getOpenUrls( KUrl("kfiledialog:///bakaar"), Phonon::BackendCapabilities::availableMimeTypes().join(" "), this, i18n("Select File to Play") );
    }

    //FIXME KIO stuff here - though Phonon seems to do a lot of it for me

    setCentralWidget(m_videoWidget);
    createPath(m_mediaPlayer, m_videoWidget);

    //uncomment for audio
    AudioOutput *audioOutput =  new AudioOutput(Phonon::VideoCategory, this);
    createPath(m_mediaPlayer, audioOutput);

    connect(m_mediaPlayer,SIGNAL(stateChanged(Phonon::State,Phonon::State)),this,SLOT(stateChanged(Phonon::State,Phonon::State)));
    //connect(m_mediaPlayer, SIGNAL(hasVideoChanged(bool)), this, SLOT(changeTitle(bool)));

    m_osd = new Osd(m_mediaPlayer, this);
    m_osd->setAttribute(Qt::WA_TranslucentBackground);
    m_osd->setWindowFlags(Qt::Tool | Qt::FramelessWindowHint);
    moveOSD();

    if (!videoUrls.isEmpty()){
        foreach(KUrl url, videoUrls){
            MediaItem mediaItem(url);
            m_osd->enqueue(mediaItem);
        }
        m_osd->playNext(); // Should play the item at the position 0 of the playlist
        MediaSource mediaSource(videoUrls.first());
        m_mediaPlayer->setCurrentSource(mediaSource);
        m_mediaPlayer->play();
    }

    m_mouseMoveTimer.setSingleShot(true);
    m_mouseMoveTimer.setInterval(OSD_HIDE_TIMEOUT );
    connect(&m_mouseMoveTimer,SIGNAL(timeout()),this,SLOT(mouseMoveTimerExpired()));
    m_mouseMoveTimer.start();
}

//the OSD cannot be shown before the main window (or bad things happen), instead it is shown just after this window is
void MainWindow::show()
{
    QMainWindow::show();
    m_osd->show();
    moveOSD();
}


MainWindow::~MainWindow()
{
}

//if the main window moves, move the OSD to the new location
void MainWindow::moveEvent(QMoveEvent*)
{
    moveOSD();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    moveOSD();
}


void MainWindow::moveOSD()
{
    //work out offset from the top left corner of the main widget that the top left corner of the OSD should be
    QPoint offset(((width() - m_osd->width())/2),height()- m_osd->height() - 10);

    //set to global co-ordinates. MainWindow screen co-ordinates + offset calculated above
    m_osd->move(pos() + offset);
}

void MainWindow::mouseMoveEvent(QMouseEvent* event)
{
    //if mouse has moved a small distance
    if((event->pos() - m_lastMousePosition).manhattanLength() > 10)
    {
        m_lastMousePosition = event->pos();
        m_mouseMoveTimer.start();

        m_osd->show();
        unsetCursor();
    }
}

void MainWindow::mouseMoveTimerExpired()
{
    //don't hide if mouse is over the OSD, even if timer has expired
    if (!m_osd->underMouse())
    {
        m_osd->hide();
        setCursor(Qt::BlankCursor);
    }
}



void MainWindow::mouseDoubleClickEvent(QMouseEvent *)
{
    m_mediaPlayer->toggleFullScreen();
}

void MainWindow::stateChanged(Phonon::State, Phonon::State)
{
    //adjust size once video size is known, only do this once, to allow user resize afterwards
    if(! m_adjustedSize)
    {
        if (m_videoWidget->sizeHint().isValid())
        {
            m_adjustedSize = true;
            adjustSize();
        }
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    //FIXME should probably check there are at least some URLs in here..
    event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent *event)
{
    //on a drag/drop load the first URL.
    QList<QUrl> urls = event->mimeData()->urls();
    if (urls.length() > 0)
    {
        m_adjustedSize = false;
        event->accept();
        m_osd->clear();
        foreach(QUrl url, urls){
            MediaItem mediaItem(url);
            m_osd->enqueue(mediaItem);
        }
        m_osd->playNext();
    }
}

QSize MainWindow::sizeHint() const
{
    //set to fit video widget OR OSD + margin
    return m_videoWidget->sizeHint().expandedTo(m_osd->minimumSize() + QSize(20,40));
}

void MainWindow::keyReleaseEvent ( QKeyEvent * event ){
    m_mediaPlayer->keyboardNavigation(event);
}
