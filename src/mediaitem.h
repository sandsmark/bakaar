#ifndef MEDIAITEM_H
#define MEDIAITEM_H

#include <KUrl>

class MediaItem
{
public:
    MediaItem();
    MediaItem(const KUrl & url);
    /**
      * Retrieves the url of the MediaItem.
      */
    KUrl url() const;
    /**
      * Sets the url of the MediaItem.
      */
    void setUrl(const KUrl & url);
private:
    KUrl m_url;
};

#endif // MEDIAITEM_H
