#ifndef OSDTIMER_H
#define OSDTIMER_H

#include <QTime>

#include "osdwidget.h"

/**
  * This class is used to display the elapsed time, remaining time of the movie.
  */
class OsdTimer : public QLabel
{
    Q_OBJECT
public:
    /**
      * Represents the time format hh:mm:ss.
      */
    static const QString timeFormat;
    /**
      * Represents the available display modes.
      */
    enum DisplayMode {elapsed, remaining, elapsed_total};
    /**
      * Default constructor.
      */
    OsdTimer(QWidget*);
    /**
      * Switches the current display mode to the next one available in a circular way.
      *
      * The default mode is "elapsed" (sets in the contructor), and then this
      * function will provide "remaining", then "both", then "elapsed", then etc...
      */
    void nextDisplayMode();
    /**
      * Gets current display mode.
      */
    DisplayMode currentDisplayMode() const;
    /**
      * Displays the time according the the current display mode.
      */
    void display(qint64 current, qint64 remaining, qint64 total);

protected:
    void mouseReleaseEvent(QMouseEvent *);

private:
    DisplayMode m_currentDisplayMode;
};

#endif // OSDTIMER_H
