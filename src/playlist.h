#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QWidget>

#include "playlistmodel.h"
#include "ui_playlist.h"

class QStandardItemModel;

/**
  * Represents the dialog to manage the playlist.
  */
class Playlist : public QDialog
{
    Q_OBJECT
public:
    Playlist(PlaylistModel *playlist, QWidget *parent = 0);
    /**
      * Retrieves the playlist.
      */
    PlaylistModel* playlist() const;
    /**
      * Sets the playlist.
      */
    void setPlaylist(PlaylistModel *playlist);
    /**
      * Returns the selected files.
      */
    QModelIndexList selectedFiles() const;
    /**
      * Appends a selected row to the current selection.
      *
      * @param row the number of the row to append to the selection
      */
    void appendToSelection(int row);

signals:
    void clicked(const QModelIndex & index);

private:
    /**
      * Represents the fixed height of a playlist button.
      */
    static const int BUTTON_HEIGHT = 32;
    /**
      * Represents the fixed width of a playlist button.
      */
    static const int BUTTON_WIDTH = 32;
    /**
      * Dialog made with the designer.
      */
    Ui::Playlist m_playlistUi;
    /**
      * The playlist data model.
      */
    PlaylistModel* m_playlist;

private slots:
    /**
      * Adds files into the playlist.
      */
    void addFiles();
    /**
      * Removes the selected files from the playlist.
      */
    void removeSelectedFiles();
    /**
      * Emits a signal containing the item which has been  double clicked on.
      *
      * @param index index of the row which was clicked.
      */
    void doubleClickedItem(const QModelIndex & index);
    /**
      * Moves the selected item one position towards the begining of the playlist.
      */
    void up();
    /**
      * Moves the selected item one position towards the end of the playlist.
      */
    void down();
    /**
      * Disables the up and/or down button when the action is not possible.
      * Kindly note that the index parameter is not used for this operation.
      */
    void disableUpDownButtons(const QModelIndex & index);
    /**
      * Clears the playlist.
      */
    void clear();
};

#endif // PLAYLIST_H
