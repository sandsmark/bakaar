#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QTimer>

#include <Phonon/VideoWidget>
#include "mediaplayer.h"

#include "osd.h"

using namespace Phonon;


class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void keyReleaseEvent ( QKeyEvent * event );
    void moveEvent(QMoveEvent *);
    void resizeEvent(QResizeEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseDoubleClickEvent (QMouseEvent *);
    void dragEnterEvent(QDragEnterEvent *);
    void dropEvent(QDropEvent *);
    QSize sizeHint() const;

public slots:
    void show();
    void stateChanged(Phonon::State, Phonon::State);
    void mouseMoveTimerExpired();

private:
    void moveOSD();
    Osd* m_osd;

    MediaPlayer* m_mediaPlayer;
    VideoWidget* m_videoWidget;
    QTimer m_mouseMoveTimer;
    bool m_adjustedSize;
    QPoint m_lastMousePosition;

    /** Time out (milliseconds) between the last mouse movement and when we should hide the OSD*/
    static const int OSD_HIDE_TIMEOUT = 4000;

};

#endif // MAINWINDOW_H
