#ifndef OSD_H
#define OSD_H

#include <QWidget>
#include <QSlider>
#include <QTimeLine>

#include "mediaplayer.h"
#include "osdtimer.h"
#include "playlist.h"
#include "playlistmodel.h"
#include "ui_osd.h"

using namespace Phonon;

class Osd : public QWidget
{
Q_OBJECT
public:
    Osd(MediaPlayer* );
    Osd(MediaPlayer*,QWidget *parent = 0);
    ~Osd();
    inline bool underMouse() {return QWidget::underMouse();}; /*Sneakily made public*/
    /**
      * Sets the application title according to the playing item.
      */
    void changeTitle();
    /**
      * Adds an item into the playlist.
      */
    void enqueue(MediaItem mediaItem);
    /**
      * Clears the playlist.
      */
    void clear();

public slots:
    //perform the hide/show..but via an awesome fadeout
    virtual void hide();
    virtual void show();
    void showPlaylist();
    /**
      * Plays the next file in the playlist if it exists.
      */
    void playNext();
    /**
      * Plays the previous file in the playlist if it exists.
      */
    void playPrevious();
    /**
      * Sets the application title according to the playing item when a new
      * file is loaded.
      */
    void changeTitle(Phonon::State, Phonon::State);
    /**
      * Fills the mediaplayer with items from the playlist if required.
      */
    void fill();
    /**
      * Plays the item on which the user clicks in the playlist.
      */
    void playClicked(const QModelIndex & index);

protected:
    //render background
    virtual void paintEvent(QPaintEvent*);
    void keyReleaseEvent ( QKeyEvent * event );

private:
    QWidget* m_parent;
    QPixmap background;
    QTimeLine *fadeOutTimeLine;
    /**
      * The OSD window
      */
    Ui::OsdUi m_ui;
    /**
      * The MediaPlayer object
      */
    MediaPlayer* m_mediaPlayer;
    /**
      * The playlist window
      */
    Playlist * m_playlistDialog;
    /**
      * The playlist
      */
    PlaylistModel* m_playlist;

private slots:
    virtual void updateFade(int);
    void tick(qint64 time);
};

#endif // OSD_H
