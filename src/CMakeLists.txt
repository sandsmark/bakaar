#find_package(KDE4 REQUIRED)

#add_definitions(${QT_DEFINITIONS} ${KDE4_DEFINITIONS})

#include_directories (${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR} ${KDE4_INCLUDES})

#include(KDE4Defaults)
#include(MacroLibrary)

set (bakaar_SRCS
  main.cpp
    mainwindow.cpp
    mediaplayer.cpp
    mediaitem.cpp
    osd.cpp
    osdwidget.cpp
    osdplaypausebutton.cpp
    osdtimer.cpp
    playlist.cpp
    playlistmodel.cpp
)

kde4_add_ui_files(bakaar_SRCS
   osd.ui
   playlist.ui
)

#kde4_add_executable(bakaar ${bakaar_SRCS})
kde4_add_executable(${PROJECT_NAME} ${bakaar_SRCS})

target_link_libraries(${PROJECT_NAME} ${KDE4_KDECORE_LIBS} ${KDE4_PHONON_LIBS}
${KDE4_KIO_LIBS})

install(FILES bakaar.desktop DESTINATION ${XDG_APPS_INSTALL_DIR})
install(TARGETS bakaar ${INSTALL_TARGETS_DEFAULT_ARGS})