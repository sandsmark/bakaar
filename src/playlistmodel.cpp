#include <QDebug>

#include <KIcon>

#include "playlistmodel.h"

PlaylistModel::PlaylistModel(QObject *parent)
    : QAbstractListModel(parent),
    m_playingItemPosition(-1)
{
}

PlaylistModel::~PlaylistModel()
{
}

QVariant PlaylistModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole){
        return m_mediaItems.value(index.row()).url().fileName();
    }

    if (role == Qt::DecorationRole){
        if (index.row() == m_playingItemPosition){
            return KIcon("media-playback-start.png");
        }
    }

    return QVariant();
}

Qt::ItemFlags PlaylistModel::flags(const QModelIndex &index) const
{
    //TODO: Check the flags
    if (index.isValid()){
        return (Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
    }

    return Qt::ItemIsDropEnabled;
}

int PlaylistModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()){
        return 0;
    } else {
        return m_mediaItems.size();
    }
}

void PlaylistModel::addMediaItem(int row,const MediaItem & mediaItem)
{
    beginInsertRows(QModelIndex(), row, row);
    m_mediaItems.insert(row, mediaItem);
    endInsertRows();
}

void PlaylistModel::enqueueMediaItem(const MediaItem & mediaItem)
{
    addMediaItem(m_mediaItems.size(), mediaItem);
}

int PlaylistModel::playingItemPosition() const
{
    return m_playingItemPosition;
}

void PlaylistModel::setPlayingItemPosition(int position)
{
    m_playingItemPosition = position;
}

bool PlaylistModel::hasNext() const
{
    return m_playingItemPosition < (m_mediaItems.size() - 1);
}

bool PlaylistModel::hasPrevious() const
{
    return m_playingItemPosition > 0;
}

MediaItem PlaylistModel::next() const
{
    MediaItem next;

    if (hasNext()){
        return (m_mediaItems.at(m_playingItemPosition + 1));
    }

    return next;
}

MediaItem PlaylistModel::previous() const
{
    MediaItem previous;

    if (hasPrevious()){
        return (m_mediaItems.at(m_playingItemPosition - 1));
    }

    return previous;
}

void PlaylistModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_mediaItems.size() - 1);
    m_mediaItems.clear();
    m_playingItemPosition = -1;
    endRemoveRows();
}

void PlaylistModel::removeMediaItem(int position)
{
    beginRemoveRows(QModelIndex(), position, position);
    m_mediaItems.removeAt(position);
    endRemoveRows();
}

MediaItem PlaylistModel::item(int position) const
{
    return m_mediaItems.at(position);
}

void PlaylistModel::move(int origin, int destination)
{
    m_mediaItems.move(origin, destination);
}

int PlaylistModel::size() const
{
    return m_mediaItems.size();
}

void PlaylistModel::swap(int origin, int destination)
{
    m_mediaItems.swap(origin, destination);
}
