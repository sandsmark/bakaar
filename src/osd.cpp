#include <QPainter>
#include <QPaintEvent>

#include <QHBoxLayout>
#include <QSlider>
#include <QTime>
#include <QDebug>

#include <KIconLoader>

#include <Phonon/SeekSlider>

#include "playlist.h"
#include "osd.h"
#include "osdplaypausebutton.h"
#include "osdtimer.h"
#include "osdwidget.h"

using namespace Phonon;

Osd::Osd(MediaPlayer* mediaPlayer,QWidget *parent)
{
    // We start to create the playlist object to be able to transfert it to the
    // other components
    m_playlist = new PlaylistModel;
    m_playlistDialog = new Playlist(m_playlist);
    m_mediaPlayer = mediaPlayer;

    m_mediaPlayer->setPlaylist(m_playlist);

    fadeOutTimeLine = new QTimeLine(750, this);
    fadeOutTimeLine->setFrameRange(0, 90);
    connect(fadeOutTimeLine, SIGNAL(frameChanged(int)), this, SLOT(updateFade(int)));

    //buffer background
    setWindowTitle("controls");
    background = UserIcon("fs_background.png");
    setFixedSize(background.size());

    m_ui.setupUi(this);

    m_ui.seekSlider->setMediaObject(m_mediaPlayer);
    m_ui.seekSlider->setIconVisible(false);
    //
    m_ui.rewindButton->setIconName("rewind");
    m_ui.forwardButton->setIconName("forward");
    m_ui.toggleFullScreenButton->setIconName("exit_fullscreen");
    m_ui.playlistButton->setIconName("playlist");
    m_ui.previousButton->setIconName("skip_previous");
    m_ui.nextButton->setIconName("skip_next");
    m_ui.playButton->setPixmap(UserIcon("fs_play.png"));
    //
    connect(m_ui.toggleFullScreenButton,SIGNAL(released()),m_mediaPlayer,SLOT(toggleFullScreen()));
    connect(m_mediaPlayer,SIGNAL(stateChanged(Phonon::State, Phonon::State)),m_ui.playButton,SLOT(stateChanged(Phonon::State, Phonon::State)));
    connect(m_mediaPlayer, SIGNAL(finished()), this, SLOT(playNext()));
    connect(m_mediaPlayer,SIGNAL(stateChanged(Phonon::State, Phonon::State)),this,SLOT(changeTitle(Phonon::State, Phonon::State)));
    connect(m_ui.playButton,SIGNAL(released()),m_mediaPlayer,SLOT(togglePlayPause()));
    connect(m_ui.playButton,SIGNAL(released()),this,SLOT(fill()));
    connect(m_ui.forwardButton,SIGNAL(held()),m_mediaPlayer,SLOT(forward()));
    connect(m_ui.rewindButton,SIGNAL(held()),m_mediaPlayer,SLOT(rewind()));
    connect(m_ui.playlistButton, SIGNAL(released()), this, SLOT(showPlaylist()));
    connect(m_ui.nextButton, SIGNAL(released()), this, SLOT(playNext()));
    connect(m_ui.previousButton, SIGNAL(released()), this, SLOT(playPrevious()));
    connect(m_playlistDialog, SIGNAL(clicked(QModelIndex)), this, SLOT(playClicked(QModelIndex)));


    QPalette palette;
    palette.setColor(QPalette::Foreground, Qt::white);

    m_ui.timer->setPalette(palette);
    connect(m_mediaPlayer, SIGNAL(tick(qint64)), this, SLOT(tick(qint64)));

    m_parent = parent;
}

Osd::~Osd()
{
    delete m_playlistDialog;
    delete m_playlist;
}

void Osd::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.drawPixmap(event->rect(),background,event->rect());
}


void Osd::show()
{
    if(!isVisible())
    {
        updateFade(0);
        fadeOutTimeLine->setDirection(QTimeLine::Forward);
        fadeOutTimeLine->start();
        QWidget::show();
    }
}

void Osd::hide()
{
    fadeOutTimeLine->setDirection(QTimeLine::Backward);
    fadeOutTimeLine->start();
}

void Osd::updateFade(int frame)
{
    setWindowOpacity(frame/100.0);
    if(frame == 0)
    {
        QWidget::hide();
    }
}

void Osd::tick(qint64 time)
{
    m_ui.timer->display(time, m_mediaPlayer->remainingTime(), m_mediaPlayer->totalTime());
}


void Osd::keyReleaseEvent ( QKeyEvent * event ){
    m_mediaPlayer->keyboardNavigation(event);
}

void Osd::showPlaylist()
{
    m_playlistDialog->show();
}

void Osd::playNext()
{
    m_mediaPlayer->playNext();
}

void Osd::playPrevious()
{
    m_mediaPlayer->playPrevious();
}

void Osd::changeTitle(Phonon::State newstate, Phonon::State){
    if (newstate == Phonon::LoadingState){
        changeTitle();
    }
}

void Osd::changeTitle(){
    QString filePath(m_mediaPlayer->currentSource().fileName());
    int lastPathSeparator = filePath.lastIndexOf("/");
    QString title(filePath.right(filePath.length() - lastPathSeparator - 1));
    m_parent->setWindowTitle(title);
}

void Osd::enqueue(MediaItem mediaItem)
{
    m_playlist->enqueueMediaItem(mediaItem);
}

void Osd::clear()
{
    m_playlist->clear();
}

void Osd::fill()
{
    if (m_mediaPlayer->currentSource().type() == Phonon::MediaSource::Empty){
        playNext();
    }
}

void Osd::playClicked(const QModelIndex & index)
{
    if (index.isValid()){
        MediaItem item = m_playlist->item(index.row());
        m_playlist->setPlayingItemPosition(index.row());
        m_mediaPlayer->setCurrentSource(item.url());
        m_mediaPlayer->play();
    }
}
