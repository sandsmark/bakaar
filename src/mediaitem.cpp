#include "mediaitem.h"

MediaItem::MediaItem()
{
}

MediaItem::MediaItem(const KUrl & url) : m_url(url)
{
}

KUrl MediaItem::url() const
{
    return m_url;
}

void MediaItem::setUrl(const KUrl & url)
{
    m_url = url;
}
