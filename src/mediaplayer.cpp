#include <QKeyEvent>

#include "mediaplayer.h"

MediaPlayer::MediaPlayer(QWidget *parent) :
        MediaObject(parent),
        m_inhibitScreensaver(0)
{
    m_mainWindow = parent;
    connect(this,SIGNAL(stateChanged(Phonon::State,Phonon::State)),this,SLOT(handleStateChange(Phonon::State,Phonon::State)));
}

MediaPlayer::~MediaPlayer()
{

}

bool MediaPlayer::isFullScreen() const
{
    return m_mainWindow->isFullScreen();
}

void MediaPlayer::toggleFullScreen()
{
    if(m_mainWindow->isFullScreen()){
        m_mainWindow->showNormal();
        emit fullScreenChanged(false);
    } else {
        m_mainWindow->showFullScreen();
        emit fullScreenChanged(true);
    }
}

void MediaPlayer::togglePlayPause()
{
    if(state() == Phonon::PlayingState){
        pause();
    } else {
        play();
    }
}

void MediaPlayer::forward()
{
    seek(currentTime() + THIRTY_SECONDS);
}

void MediaPlayer::rewind()
{
    seek(currentTime() - THIRTY_SECONDS);
}

void MediaPlayer::handleStateChange(Phonon::State newState, Phonon::State)
{
    if(newState == Phonon::PlayingState){
        if(m_inhibitScreensaver){
            delete m_inhibitScreensaver;
            m_inhibitScreensaver = 0;
        }
        m_inhibitScreensaver = new KNotificationRestrictions(KNotificationRestrictions::ScreenSaver);
    } else {
        if(m_inhibitScreensaver){
            delete m_inhibitScreensaver;
            m_inhibitScreensaver = 0;
        }
    }
}

void MediaPlayer::longForward()
{
    seek(currentTime() + TEN_MINUTES);
}

void MediaPlayer::longRewind()
{
    seek(currentTime() - TEN_MINUTES);
}

void MediaPlayer::keyboardNavigation(QKeyEvent * event)
{
    if (event->key() == Qt::Key_Right){
        forward();
    } else if (event->key() == Qt::Key_Left){
        rewind();
    } else if (event->key() == Qt::Key_Up){
        longForward();
    } else if (event->key() == Qt::Key_Down){
        longRewind();
    } else if (event->key() == Qt::Key_Space){
        togglePlayPause();
    } else if (event->key() == Qt::Key_F){
        toggleFullScreen();
    } else if (event->key() == Qt::Key_PageUp){
        playPrevious();
    } else if (event->key() == Qt::Key_PageDown){
        playNext();
    }
}

void MediaPlayer::playNext()
{
    if (m_playlist->hasNext()){
        setCurrentSource(m_playlist->next().url());
        m_playlist->setPlayingItemPosition(m_playlist->playingItemPosition() + 1);
        play();
    }
}

void MediaPlayer::playPrevious()
{
    if (m_playlist->hasPrevious()){
        setCurrentSource(m_playlist->previous().url());
        m_playlist->setPlayingItemPosition(m_playlist->playingItemPosition() - 1);
        play();
    }
}

PlaylistModel* MediaPlayer::playlist() const
{
    return m_playlist;
}

void MediaPlayer:: setPlaylist(PlaylistModel* playlist)
{
    m_playlist = playlist;
}
