#ifndef OSDWIDGETPLAYPAUSE_H
#define OSDWIDGETPLAYPAUSE_H

#include <QLabel>
#include <Phonon/MediaObject>

using namespace Phonon;

class OsdPlayPauseButton : public QLabel
{
    Q_OBJECT
public:
    OsdPlayPauseButton(QWidget*);

protected:
    void mouseReleaseEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *);

private:
    QPixmap m_playNormal;
    QPixmap m_playHighlighted;
    QPixmap m_pauseNormal;
    QPixmap m_pauseHighlighted;
    /**
      * Set to true if playing, false if not.
      */
    bool m_isPlaying;
    bool m_neverPlayed;

signals:
    void released();

public slots:
    void stateChanged(Phonon::State, Phonon::State);

};

#endif
