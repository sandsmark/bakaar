#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QWidget>

#include <Phonon/MediaObject>

#include <KNotificationRestrictions>

#include "playlistmodel.h"

using namespace Phonon;

/** This class adds a few application specific features to the standard media object
*/

class MediaPlayer : public MediaObject
{
    Q_OBJECT
public:
    /**
      * Represents 30 seconds in milliseconds.
      */
    static const int THIRTY_SECONDS = 30000;
    /**
      * Represents 10 minutes in milliseconds.
      */
    static const int TEN_MINUTES = 600000;
    explicit MediaPlayer(QWidget *parent = 0);
    ~MediaPlayer();
    /**
      * Returns true if the MediaItem is playing in full screen mode, false otherwise.
      */
    virtual bool isFullScreen() const;
    /**
      * Allows the user to navigate within the movie using the keyboard.
      */
    void keyboardNavigation(QKeyEvent * event);
    /**
      * Retrieves the playlist.
      */
    PlaylistModel* playlist() const;
    /**
      * Sets the playlist.
      */
    void setPlaylist(PlaylistModel* playlist);

signals:
    void fullScreenChanged(bool isFullScreen);

public slots:
    /**
      * Goes 30 seconds forward within the playing item.
      */
    virtual void forward();
    /**
      * Goes 10 minutes forward within the playing item.
      */
    void longForward();
    /**
      * Goes 10 minutes backward within the playing item.
      */
    void longRewind();
    /**
      * Plays the next item in the playlist if it exists.
      */
    void playNext();
    /**
      * Plays the previous item in the playlist if it exists.
      */
    void playPrevious();
    /**
      * Goes 30 seconds backward within the playing item.
      */
    virtual void rewind();
    /**
      * Switches between window mode and fullscreen mode.
      */
    virtual void toggleFullScreen();
    /**
      * Switches between play and pause;
      */
    virtual void togglePlayPause();

private slots:
    virtual void handleStateChange(Phonon::State,Phonon::State);

private:
    QWidget* m_mainWindow;
    KNotificationRestrictions* m_inhibitScreensaver;
    /**
      * The playlist data model.
      */
    PlaylistModel* m_playlist;
};

#endif // MEDIAPLAYER_H
