#ifndef OSDWIDGET_H
#define OSDWIDGET_H

#include <QLabel>
#include <QTimer>


//subclass for a generic button. Draws custom theme.
//caches relevant pixmaps
//emits signal when clicked
class OsdWidget : public QLabel
{
    Q_OBJECT
public:
    OsdWidget(QWidget*);
    void setIconName(QString);

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

private:
    QPixmap m_iconNormal;
    QPixmap m_iconHighlighted;
    QTimer  m_mouseHeldTimer;

signals:
    void released();
    void held();
};

#endif // OSDWIDGET_H
