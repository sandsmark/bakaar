#include <Phonon/BackendCapabilities>

#include <KDirLister>
#include <KFileDialog>
#include <KIcon>
#include <KUrl>

#include "mediaitem.h"
#include "playlist.h"

Playlist::Playlist(PlaylistModel *playlist, QWidget *parent): QDialog(parent)
{
    m_playlistUi.setupUi(this);

    m_playlistUi.addFileButton->setIcon(KIcon("media-track-add-amarok.png"));
    m_playlistUi.addFileButton->setText("");
    m_playlistUi.addFileButton->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_playlistUi.addFileButton->setToolTip(i18n("Add a media file"));


    m_playlistUi.removeFileButton->setIcon(KIcon("media-track-remove-amarok.png"));
    m_playlistUi.removeFileButton->setText("");
    m_playlistUi.removeFileButton->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_playlistUi.removeFileButton->setToolTip(i18n("Remove a media file"));

    m_playlistUi.clearPlaylistButton->setIcon(KIcon("clear-playlist-amarok.png"));
    m_playlistUi.clearPlaylistButton->setText("");
    m_playlistUi.clearPlaylistButton->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_playlistUi.clearPlaylistButton->setToolTip(i18n("Clear playlist"));

    m_playlistUi.upButton->setIcon(KIcon("go-up"));
    m_playlistUi.upButton->setText("");
    m_playlistUi.upButton->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_playlistUi.upButton->setToolTip(i18n("Move an item up"));
    m_playlistUi.upButton->setEnabled(false);

    m_playlistUi.downButton->setIcon(KIcon("go-down"));
    m_playlistUi.downButton->setText("");
    m_playlistUi.downButton->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_playlistUi.downButton->setToolTip(i18n("Move an item down"));
    m_playlistUi.downButton->setEnabled(false);

    m_playlist = playlist;

    m_playlistUi.listView->setModel(m_playlist);
    m_playlistUi.listView->setSelectionMode(QAbstractItemView::MultiSelection);

    connect(m_playlistUi.addFileButton, SIGNAL(clicked()), this, SLOT(addFiles()));
    connect(m_playlistUi.clearPlaylistButton, SIGNAL(clicked()), m_playlist, SLOT(clear()));
    connect(m_playlistUi.clearPlaylistButton, SIGNAL(clicked()), this, SLOT(clear()));
    connect(m_playlistUi.removeFileButton, SIGNAL(clicked()), this, SLOT(removeSelectedFiles()));
    connect(m_playlistUi.listView, SIGNAL(clicked(QModelIndex)), this, SLOT(disableUpDownButtons(QModelIndex)));
    connect(m_playlistUi.listView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(doubleClickedItem(QModelIndex)));
    connect(m_playlistUi.upButton, SIGNAL(clicked()), this, SLOT(up()));
    connect(m_playlistUi.downButton, SIGNAL(clicked()), this, SLOT(down()));
}

PlaylistModel* Playlist::playlist() const
{
    return m_playlist;
}

void Playlist::setPlaylist(PlaylistModel *playlist)
{
    m_playlist = playlist;
}

void Playlist::addFiles()
{
    KUrl::List files;

    files = KFileDialog::getOpenUrls( KUrl("kfiledialog:///bakaar"), Phonon::BackendCapabilities::availableMimeTypes().join(" "), this, i18n("Select File to Play") );
    foreach(KUrl file, files){
        MediaItem tmpMediaItem(file);
        m_playlist->enqueueMediaItem(tmpMediaItem);
    }

    disableUpDownButtons(QModelIndex());
}

void Playlist::removeSelectedFiles()
{
    QModelIndexList selection = m_playlistUi.listView->selectionModel()->selectedIndexes();
    foreach(QModelIndex index, selection){
        m_playlist->removeMediaItem(index.row());
    }
    m_playlistUi.listView->selectionModel()->clearSelection();

    disableUpDownButtons(QModelIndex());
}

void Playlist::doubleClickedItem(const QModelIndex & index)
{
    emit clicked(index);
}

//TODO: make it possible to move a selection of items
void Playlist::up()
{
    QModelIndexList selection = m_playlistUi.listView->selectionModel()->selectedIndexes();

    if (selection.size() == 1){
        QModelIndex index = selection.first();
        int selected = index.row();
        if (index.row() > 0){
            int destination = index.row() - 1;
            int playingRow = m_playlist->playingItemPosition();
            m_playlist->move(selected, destination);
            if (selected == playingRow){
                m_playlist->setPlayingItemPosition(destination);
            }
            if (selected == playingRow + 1){
                m_playlist->setPlayingItemPosition(selected);
            }
            m_playlistUi.listView->selectionModel()->clearSelection();
            appendToSelection(destination);
        }
    }
}

//TODO: make it possible to move a selection of items
void Playlist::down()
{
    QModelIndexList selection = m_playlistUi.listView->selectionModel()->selectedIndexes();

    if (selection.size() == 1){
        QModelIndex index = selection.first();
        int selected = index.row();
        if (selected < (m_playlist->size() - 1)){
            int destination = index.row() + 1;
            int playingRow = m_playlist->playingItemPosition();
            m_playlist->move(selected, destination);
            if (selected == playingRow){
                m_playlist->setPlayingItemPosition(destination);
            }
            if (selected == playingRow - 1){
                m_playlist->setPlayingItemPosition(selected);
            }
            m_playlistUi.listView->selectionModel()->clearSelection();
            appendToSelection(destination);
        }
    }
}

void Playlist::appendToSelection(int row)
{
    if (!m_playlistUi.listView->selectionModel()){
        return;
    }

    QItemSelection selection;
    QModelIndex tl = m_playlistUi.listView->model()->index(row, 0);
    QModelIndex br(tl);
    selection.append(QItemSelectionRange(tl, br));
    m_playlistUi.listView->selectionModel()->select(selection, QItemSelectionModel::Select);

}

void Playlist::disableUpDownButtons(const QModelIndex &)
{
    QModelIndexList selection = m_playlistUi.listView->selectionModel()->selectedIndexes();

    if ( selection.size() != 1){
        m_playlistUi.upButton->setEnabled(false);
        m_playlistUi.downButton->setEnabled(false);
        return;
    }

    int selected = selection.first().row();

    m_playlistUi.upButton->setEnabled(true);
    m_playlistUi.downButton->setEnabled(true);

    if (selected == 0){
        m_playlistUi.upButton->setEnabled(false);
    }
    if (selected == m_playlist->size() - 1){
        m_playlistUi.downButton->setEnabled(false);
    }
}

void Playlist::clear()
{
    m_playlist->clear();
    disableUpDownButtons(QModelIndex());
}
