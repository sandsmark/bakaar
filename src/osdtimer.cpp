#include "osdtimer.h"

const QString OsdTimer::timeFormat = QString("hh:mm:ss");

OsdTimer::OsdTimer(QWidget*parent) : QLabel(parent)
{
    m_currentDisplayMode = remaining;
}

void OsdTimer::nextDisplayMode()
{
    switch(m_currentDisplayMode)
    {
    case elapsed:
        m_currentDisplayMode = remaining;
        break;
    case remaining:
        m_currentDisplayMode = elapsed_total;
        break;
    case  elapsed_total:
        m_currentDisplayMode = elapsed;
        break;
    }
}

void OsdTimer::mouseReleaseEvent(QMouseEvent *)
{
    nextDisplayMode();
}

OsdTimer::DisplayMode OsdTimer::currentDisplayMode() const
{
    return m_currentDisplayMode;
}

void OsdTimer::display(qint64 current, qint64 remaining, qint64 total)
{
    QTime currentTime;
    QTime remainingTime;
    QTime totalTime;

    currentTime = currentTime.addMSecs(current);
    remainingTime = remainingTime.addMSecs(remaining);
    totalTime = totalTime.addMSecs(total);

    if (currentDisplayMode() == OsdTimer::elapsed){
        setText(currentTime.toString(timeFormat));
    } else if (currentDisplayMode() == OsdTimer::remaining){
        setText("- "+remainingTime.toString(timeFormat));
    } else if (currentDisplayMode() == OsdTimer::elapsed_total){
        setText(currentTime.toString(timeFormat)+" / "+totalTime.toString(timeFormat));
    }
}
