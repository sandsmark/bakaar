#include <QMouseEvent>
#include <QDebug>
#include <KIconLoader>
#include "osdwidget.h"

OsdWidget::OsdWidget(QWidget* parent) : QLabel(parent),
m_mouseHeldTimer(this)
{
    m_mouseHeldTimer.setInterval(100);//timer stores how regularly we send help() signal whilst mouse is down
    m_mouseHeldTimer.setSingleShot(false);
    connect(&m_mouseHeldTimer,SIGNAL(timeout()),this,SIGNAL(held()));
}

void OsdWidget::setIconName(QString name)
{
    m_iconNormal = UserIcon("fs_"+name+".png");
    m_iconHighlighted = UserIcon("fs_"+name+"_highlight.png");

    setPixmap(m_iconNormal);
}

void OsdWidget::mousePressEvent(QMouseEvent *)
{
    setPixmap(m_iconHighlighted);
    m_mouseHeldTimer.start();
    emit held(); //emit when pressing starts, and then at regular intervals from the timer
}

void OsdWidget::mouseReleaseEvent(QMouseEvent *)
{
    m_mouseHeldTimer.stop();
    setPixmap(m_iconNormal);
    emit released();
}

