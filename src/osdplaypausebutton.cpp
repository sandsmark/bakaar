#include <QMouseEvent>
#include <QDebug>
#include <KIconLoader>
#include "osdplaypausebutton.h"

OsdPlayPauseButton::OsdPlayPauseButton(QWidget* parent) : QLabel(parent)
{
    m_playNormal = UserIcon("fs_play.png");
    m_pauseNormal = UserIcon("fs_pause.png");
    m_playHighlighted = UserIcon("fs_play_highlight.png");
    m_pauseHighlighted =  UserIcon("fs_pause_highlight.png");

    m_isPlaying = false;
    m_neverPlayed = true;
}

void  OsdPlayPauseButton::mousePressEvent(QMouseEvent *){
    if (m_isPlaying){
        setPixmap(m_playHighlighted);
    } else  if (!m_isPlaying && !m_neverPlayed){
        setPixmap(m_pauseHighlighted);
    }
}

void OsdPlayPauseButton::mouseReleaseEvent(QMouseEvent *)
{
    emit(released());
}

void OsdPlayPauseButton::stateChanged(Phonon::State newState, Phonon::State)
{
    if(newState == Phonon::PlayingState)
    {
        setPixmap(m_pauseNormal);
        m_isPlaying = false;
        m_neverPlayed = false;
    }
    else
    {
        setPixmap(m_playNormal);
        m_isPlaying = true;
    }
}
