set(PICS
        fs_background.png
        fs_exit_fullscreen_highlight.png
        fs_exit_fullscreen.png
        fs_forward_highlight.png
        fs_forward.png
        fs_pause_highlight.png
        fs_pause.png
        fs_play_highlight.png
        fs_play.png
        fs_playlist_highlight.png
        fs_playlist.png
        fs_rewind_highlight.png
        fs_rewind.png
        fs_skip_previous_highlight.png
        fs_skip_previous.png
        fs_skip_next_highlight.png
        fs_skip_next.png
        clear-playlist-amarok.png
        media-track-add-amarok.png
        media-track-remove-amarok.png
)

message(STATUS "DATA_INSTALL_DIR: ${DATA_INSTALL_DIR}")
message(STATUS "ICON_INSTALL_DIR: ${ICON_INSTALL_DIR}")

install(FILES ${PICS} DESTINATION "${DATA_INSTALL_DIR}/bakaar/pics")
